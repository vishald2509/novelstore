<html>
  <head>
      <style>
          
          /* ##########################Main########################## */
          /* ##########################Body########################## */
          a{
              color: inherit;
              text-decoration: none;
          }
          .nsbody{
              background-color: #6a2b96;
              height: 520px;
              margin-top: 40px;
              margin-bottom: 40px;
              padding-top: 20px;
              padding-bottom: 20px;
          }
          .nsbodyleft{
              display:inline-block;
              height: 100%;    
              color: white;
              padding: 10px 10px 10px 10px;
          }
          
          /* ###############################TableSection################################### */
          
          td{
              color: white;
          }
           table{
               border-collapse:separate;
               border-spacing:10px 15px;
          }
          
          /* ############################################################################# */
          
          .nsbodyleftquote{
              display:inline-block;
              background-color: white;
              margin-top: -20px;
              width: 350px;
              height: 60px;
              vertical-align: middle;
              margin-left: 100px;
              font-family:  "Comic Sans MS";
              font-size: 20PX;
              text-align: center;
              font-weight: 900;
              padding: 10px 10px 10px 10px;
          }
          /* ##########################loginpart########################## */
          .nsbodyright{
              display:inline-block;
              width: 400px;
              float: right;
              background-color: white;
              height: 100%;
              color: black;
          }
          .fontblack{
              color: black;
          }
          .nslogintable
          {
              margin: 150px 0px 150px 80px;
          }
          .nsloginhead 
          {
              font-weight: bold;
              font-size: 35px;
          }
          
          .nsfontsmaller
          {
              font-size: 15px;
          }
          .nsloginlabel
          {
              font-weight: bold;
          }
          /* ##########################loginpart########################## */
      </style>
      <link rel="stylesheet" type="text/css" href="css/nsstyle.css">
  </head>
    <body>
        <div>
            <div class="nsheader">
                <div class="logo"><a href="welcome.php">Novel Store</a></div>
                <div class="tabs"><a href="adminaccount.php">Account</a></div>
                <div class="tabs"><a href="#">Contact us</a></div>
                <div class="tabs"><a href="adminmanagement.php">Inventory</a></div>
                <div class="tabs"><a href="welcome.php">Home</a></div>
            </div>
            <div class="nsbody">
            <div class="nsbodyleft">
                <form action="signupinsert.php" method="get">
                    <center><h2 >Add New Admin</h2></center>
                    <table >
                        <tr>
                            <td><label>First Name: </label></td><td><input type="text" name="fname"></td>
                            <td><label>Last Name: </label></td><td><input type="text" name="lname"></td>
                        </tr>
                        <tr><td colspan=""><label>Date of birth:</label></td><td colspan="3"><input type="text" name="dob"></td></tr>
                        <tr><td colspan=""><label>Gender:</label></td><td colspan="3"><input type="radio" name="gender" value="male"><label>Male</label><input type="radio" name="gender" value="female"><label>Female</label></td></tr>
                        <tr><td colspan=""><label>Phone No:</label></td><td colspan="3"><input type="text" name="phno"></td></tr>
                        <tr>
                            <td><label>UserName:</label></td><td><input type="text" name="username"></td>
                            <td><label>Email:</label></td><td><input type="text" name="email"></td>
                        </tr>
                        <tr>
                            <td><label>Password: </label></td><td><input type="password" name="pass"></td>
                            <td><label>Verifie Password: </label></td><td><input type="password" name="vpass"></td>
                        </tr>
                        
                        <tr><td colspan=""><label>Address:</label></td><td colspan="3"><textarea rows="4" cols="35" name="address"></textarea></td></tr>
                        <tr><td colspan=""></td><td><input type="submit" value="Submit" class="nsbtn"> </td><td><input type="reset" class="nsbtn" ></td></tr>
                    </table>                    
                </form>
            </div>
            <div class="nsbodyright">
                
                
            </div> 
                
            </div>
            <div class="nsfooter">
             
            </div>
        </div>
    </body>
    
</html>