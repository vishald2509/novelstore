<html>
    <head>
        <style>

            /* ##########################Main########################## */

            /* ##########################Header########################## */
            .nsheader{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 40px;
                display:inline-block;
            }
            .tabs
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 100px;
                padding-right: 100px;
                margin-top: 7px;
            }
            .logo{
                display:inline-block;
                color:whitesmoke;
                margin-top: 7px;
                margin-left: 50px;
                font-size: 20;
                font-weight: 1000;
            }
            /* ##########################Body########################## */
            a{
                color: inherit;
                text-decoration: none;
            }
            .nsbody{
                background-color: #6a2b96;
                min-height: 520px;
                margin-top: 40px;
                margin-bottom: 40px;
                padding-top: 20px;
                padding-bottom: 20px;
                color: white;

            }

            .nsbodyheader{
                display:inline-block;
                align-content: center;
                background-color: white;
                margin-top: -20px;
                width: 350px;
                height: 60px;
                vertical-align: middle;
                text-align: center;
                align-content: center;
                font-family:  "Comic Sans MS";
                font-size: 20PX;
                text-align: center;
                font-weight: 900;
                position: absolute;
                z-index: 0;
                left: 40%;
                padding: 10px 10px 10px 10px;    
                color: #6a2b96;
            }

            /* ###############################TableSection################################### */

            td{
                color: white; 
                font-weight: 800;
            }
            .color1
            {
                color: #6a2b96;

            }
            table{
                background-color: white;
                min-width: 400px;
                padding-left: 100px;
                padding-right: 100px;
                border-collapse:separate;
                border-spacing:10px 15px;
                border-color: #6a2b96;
                background: linear-gradient(to right, white 210px, #4a1e69 200px, #4a1e69);
            }


            /* ############################################################################# */

            .inventory
            {
                // background-color: white;
                width: 700px;
                margin-top: 130px;
                margin-left: 430px;


            }
            .btnContainer
            {
                width: 700px;
            }
            .nsinlineblock
            {

                display: inline-block;
            }
            .nsdivbig
            {
                margin-top: 10px;
                margin-bottom: 30px;
                background-color: #74D600;
                margin-left: 50px;
                margin-right: 50px;
                padding: 20px 40px 20px 40px ;
                font-size: 25px;
            }
            .nsdivbigsolo
            {
                margin-top: 10px;
                margin-bottom: 30px;
                background-color: #74D600;

                width: 461px;
                height: 40px;
                padding: 10px 0px 10px 0px ;
                font-size: 25px;
            }
            /* ########################## button ########################## */
            .nsbtn
            {
                margin-bottom: 10px;
                margin-left: 10px;
                margin-right: 10px;
                font-weight: bold;
                border: 0;
                padding-left: 10px;
                padding-right: 10px;
                height: 25px;
                background-color: #4a1e69;
                color: white;
            }
            .nsaddcart
            {
                background-color: #74D600 !important;
            }

            .nshandlerbtn{
                float: right;
            }


            /* ##########################footer########################## */
            .nsfooter
            {
                height: 40px;
            }

        </style>
        <style>
            .logotitle{
                display:inline-block;
                color:#4a1e69;

                margin-top: 7px;
                margin-left: 50px;
                font-size: 60;
                width: 600px;
                font-weight: 1000;
            }
            .logoimage{
                width: 100px;
                height: 100px;

            }
            .logo
            {
                padding: 10px 10px 10px 10px;
                background-color:#4a1e69;
                position: relative;
                top: -60px;
            }
            .tabsindex
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 20px; 
                padding-right: 30px; 

                margin-top: 7px;
            }
            .nsheaderindex{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 10px;

                display:inline-block;

            }
            .logoquote{
                display:inline-block;
                background-color: white;
                margin-top: -30px;
                border: solid;
                padding: 10px;
                border-color: #4a1e69;
                border-width: 5px;
                width: 200px;
                height: 60px;
                vertical-align: middle;
                margin-left: -0px; 
                font-family:  "Comic Sans MS";
                font-size: 15PX;
                text-align: center;
                font-weight: 900;
                color: black;
                padding: 10px 10px 10px 10px;
            }



        </style>

        <link rel="stylesheet" type="text/css" href="css/dropdown.css">
    </head>
    <body>
        <?php

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        if(!$_SESSION["uname"])
        {
            header("Location: index.php");
        }
        else
        {

            $uname=$_SESSION["uname"];
        }
        ?>

        <div>
            <div>
                <input class="nsbtn nshandlerbtn" type="button" value="Logout" onclick="location.href = 'logout.php'">
                <center><div class="logotitle"><a href="index.php">Novel Store</a></div></center>
            </div>
            <div class="nsheaderindex">
                <div class="logo"><img src="images/logoimage.png" class="logoimage"></div>
                <div class="tabsindex"><?php include 'include/accountdd.php';?></div>
                <div class="tabsindex"><a href="#">Contact us</a></div>
                <div class="tabsindex"><?php include 'include/dropdown.php';?></div>
                <div class="tabsindex"><a href="welcome.php">Home</a></div>
            </div>
            <div class="nsbody">
                <div class="logoquote">A WORLD OF BOOKS IN THE PALM OF YOUR HAND</div>
                <div class="nsbodyheader">My Profile</div>

                <div class="inventory">

                    <center>
                        <div clas="btnContainer">
                            <div class="nsinlineblock "><div class="nsinlineblock nsdivbig"><a href="cart.php">My Cart</a></div><div class="nsinlineblock nsdivbig"><a href="orders.php">My Orders</a></div></div>
                            <div class="nsdivbigsolo"><a href="#">FeedBack</a></div>
                        </div>

                        <table>

                            <?php

                            $servername = "localhost";
                            $username = "root";
                            $password = "";
                            $dbname = "novelstore";

                            // Create connection
                            $conn = new mysqli($servername, $username, $password, $dbname);
                            // Check connection
                            if ($conn->connect_error) {
                                die("Connection failed: " . $conn->connect_error);
                            } 

                            $sql = "SELECT * FROM customertable";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo "<tr><td class='color1' align='right'>First Name:</td><td>". $row["fname"]. "</td></tr>";
                                    echo "<tr><td class='color1' align='right'>Last Name:</td><td>" . $row["lname"]. "</td></tr>";
                                    echo "<tr><td  class='color1' align='right'>Date of birth:</td><td>" . $row["dob"]. "</td></tr>";
                                    echo "<tr><td class='color1' align='right' >Gender:</td><td>" . $row["gender"]. "</td></tr>";
                                    echo "<tr><td class='color1' align='right'>Phone No:</td><td>" . $row["phno"]. "</td></tr>";
                                    echo "<tr><td class='color1' align='right'>Username:</td><td>" . $row["username"]. "</td></tr>";
                                    echo "<tr><td class='color1' align='right'>Email</td><td>" . $row["email"]. "</td></tr>";
                                    echo "<tr><td class='color1' align='right'>Address</td><td>" . $row["address"]. "</td></tr>";
                                    echo '</tr>';
                                }
                            } else {
                                echo "0 results";
                            }
                            $conn->close();

                            ?>

                        </table>
                    </center>

                </div>
                <div class="nsfooter">
                </div>
            </div>
        </div>

    </body>
</html>