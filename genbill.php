
<?php

session_start();
if(!$_SESSION["uname"])
{
    header("Location: index.php");
}
else
{
    $uname=$_SESSION["uname"];
    $user_id=$_SESSION["uid"];
}
?>
<html>
    <head>
        <title>Weclome</title>
        <style>

            /* ##########################Main########################## */

            /* ##########################Header########################## */
            .nsheader{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 40px;

                display:inline-block;

            }
            .tabs
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 100px;
                padding-right: 100px;

                margin-top: 7px;
            }
            .logo{
                display:inline-block;
                color:whitesmoke;
                margin-top: 7px;
                margin-left: 50px;
                font-size: 20;
                font-weight: 1000;
            }
            /* ##########################Body########################## */
            a{
                color: inherit;
                text-decoration: none;
            }
            .nsbody{
                background-color: #6a2b96;
                height: 520px;
                margin-top: 40px;
                margin-bottom: 40px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .nsbodyheader{
                display:inline-block;
                align-content: center;
                background-color: white;
                margin-top: -20px;
                width: 350px;
                height: 60px;
                vertical-align: middle;
                text-align: center;
                align-content: center;
                font-family:  "Comic Sans MS";
                font-size: 20PX;
                text-align: center;
                font-weight: 900;
                position: absolute;
                z-index: 1;
                left: 40%;
                padding: 10px 10px 10px 10px;                
            }
            .nsbodyleft{
                display:inline-block;
                height: 100%;
                margin-top: 80px;
                margin-left: 80px;
                margin-right: 80px;
                color: white;
            }

            /* ###############################TableSection################################### */

            td{
                color: white;
            }
            table{
                border-collapse:separate;
                border-spacing:10px 15px;
            }

            /* ############################################################################# */



            /* ##########################loginpart########################## */
            .fontblack{
                color: black;
            }
            .nslogintable
            {
                margin: 150px 0px 150px 80px;
            }
            .nsloginhead 
            {
                font-weight: bold;
                font-size: 35px;
            }

            .nsfontsmaller
            {
                font-size: 15px;
            }
            .nsloginlabel
            {
                font-weight: bold;
            }

            /* ##########################loginpart########################## */



            .nsbtn
            {
                font-weight: bold;
                border: 0;
                min-width: 80px;
                height: 25px;
                background-color: #4a1e69;
                color: white;
            }
            .nshandlerbtn{
                float: right;
            }
            .nsaddcart
            {
                background-color: #74D600 !important;
            }

            /* ##########################footer########################## */

            .nsfooter
            {
                height: 40px;
            }
        </style>
    </head>
    <body>
        <div>
            <input class="nsbtn nshandlerbtn" type="button" value="Logout" onclick="location.href = 'logout.php'">
        </div>
        <div>
            <div class="nsheader">
                <div class="logo"><a href="welcome.php">Novel Store</a></div>
                <div class="tabs"><?php include 'include/dropdown.php';?></div>
                <div class="tabs"><a href="#">Contact us</a></div>
                <div class="tabs"><a href="inventory.php">Inventory</a></div>
                <div class="tabs"><a href="welcome.php">Home</a></div>
            </div>
            <div class="nsbody">
                <div class="nsbodyheader">BILL Generated</div>
                <div class="nsbodyleft">
                    <form method="get" action="insertbill.php">
                        <table>
                            <tr><td>Name</td><td>author</td><td>quantity</td><td>price</td><td>image</td></tr>
                            <?php

                            $servername = "localhost";
                            $username = "root";
                            $password = "";
                            $dbname = "novelstore";

                            // Create connection
                            $conn = new mysqli($servername, $username, $password, $dbname);
                            // Check connection
                            if ($conn->connect_error) {
                                die("Connection failed: " . $conn->connect_error);
                            } 

                            //$sql ="INSERT INTO carttable( bid, uid, bname, bauthor, price, imgpic) VALUES ('.$bid.', '.$uid.' ,'.$bname.', '.$bauthor.', '.$price.', '.$imgpic.')";
                            $sql = "SELECT citemid, bid, uid, bname, bauthor, quantity, price, imgpic FROM carttable";
                            $result = $conn->query($sql);
                            $total=0;
                            $amount=0;
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo "<tr><td>". $row["bname"]. "</td>";
                                    echo "<td>" . $row["bauthor"]. "</td>";
                                    echo "<td>" . $row["quantity"]. "</td>";
                                    echo "<td>" . $row["price"]. "</td>";
                                    $a= ''.$row["imgpic"].'';
                                    echo '<td><img src="images/'.$a.'" alt="'.$a.'"></td>';
                                    echo "<td>" . $amount= $row["price"]*$row["quantity"]. "</td></tr>";
                                    
                                    $total = $total+$amount;
                                }
                            } else {
                                echo "0 results";
                            }

                            echo "<tr><td colspan='2'></td><td align='right'>Total</td><td>$total</td>";
                            echo "<td align='right'><input id='btnadd' class='nsaddcart nsbtn' type='submit' value='Generate Bill' ></td>
                            </tr>";

                            $conn->close();

                            ?>    


                        </table>

                    </form>

                </div>
            </div>
            <div class="nsfooter">

            </div>
        </div>
    </body>

</html>