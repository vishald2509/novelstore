<html>
    <head>
        <style>

            /* ##########################Main########################## */

            /* ##########################Header########################## */
            .nsheader{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 40px;          
                display:inline-block;

            }
            .tabs
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 100px;
                padding-right: 100px;

                margin-top: 7px;
            }  .logo{
                display:inline-block;
                color:whitesmoke;
                margin-top: 7px;
                margin-left: 50px;
                font-size: 20;
                font-weight: 1000;
            }
            /* ##########################Body########################## */
            a{
                color: inherit;
                text-decoration: none;
            }
            .nsbody{
                background-color: #6a2b96;
                height: 520px;
                margin-top: 40px;
                margin-bottom: 40px;
                padding-top: 20px;
                padding-bottom: 20px;
                color: white;

            }

            /* ###############################TableSection################################### */

            td{
                color: white;
            }
            table{
                border-collapse:separate;
                border-spacing:10px 15px;
            }

            /* ############################################################################# */

            .uploader
            {
                // background-color: white;
                width: 700px;
                margin-top: 30px;
                margin-left: 30px;

            }
            /* ##########################footer########################## */
            .nsfooter
            {
                height: 40px;
            }

        </style>
        <script>
            function loadthis()
            {
                <?php
                    session_start();
                    if(isset($_SESSION["error"]))
                    {
                        $warning = $_SESSION["error"];
                        
                    }else
                    {
                        $warning = "#FFFFFF";
                    }
                
                    
                ?>
                document.getElementById("imgwarning").style.color="<?php echo $warning; ?>";
                
            }
        </script>
    </head>
    <body onload="loadthis()">

        <div class="nsheader">
            <div class="logo"><a href="adminwelcome.php">Novel Store</a></div>
            <div class="tabs"><a href="adminaccount.php">Account</a></div>
            <div class="tabs"><a href="#">Contact us</a></div>
            <div class="tabs"><a href="adminmanagement.php">Managment</a></div>
            <div class="tabs"><a href="adminwelcome.php">Home</a></div>
        </div>
        <div class="nsbody">
            <br><br>
            <div class="uploader">
                <h3>Upload new book to the inventory</h3><br>
                <form enctype="multipart/form-data" action="upload.php" method="POST">
                    <table>
                        <tr><td><label>Name of the Book:</label></td><Td><input type="text" name="name" ></Td></tr>
                        <tr><td><label>Book Author:</label></td><td><input type="text" name="author" ></td></tr>
                        <tr><Td><label>Quantity:</label></Td><td><input type="text" name="quantity" ></td></tr>
                        <tr><Td><input type="hidden" name="MAX_FILE_SIZE" value="512000" /><label>Upload image:</label></Td><td><input name="userfile" type="file" /><label id="imgwarning">image file should be 500kb or smaller size</label></td></tr>
                        <tr><td></td><td ><input type="submit" value="ADD BOOK" /></td></tr>
                    </table>
                </form>
            </div>
            <div class="nsfooter">

            </div>
        </div>
    </body>

</html>