<html>
    <head>
        <style>

            /* ##########################Main########################## */

            /* ##########################Header########################## */
            .nsheader{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 40px;
                display:inline-block;
            }
            .tabs
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 100px;
                padding-right: 100px;
                margin-top: 7px;
            }
            .logo{
                display:inline-block;
                color:whitesmoke;
                margin-top: 7px;
                margin-left: 50px;
                font-size: 20;
                font-weight: 1000;
            }
            /* ##########################Body########################## */
            a{
                color: inherit;
                text-decoration: none;
            }
            .nsbody{
                background-color: #6a2b96;
                min-height: 520px;
                margin-top: 40px;
                margin-bottom: 40px;
                padding-top: 20px;
                padding-bottom: 20px;
                color: white;

            }
            
            .nsbodyheader{
                display:inline-block;
                align-content: center;
                background-color: white;
                margin-top: -20px;
                width: 350px;
                height: 60px;
                vertical-align: middle;
                text-align: center;
                align-content: center;
                font-family:  "Comic Sans MS";
                font-size: 20PX;
                text-align: center;
                font-weight: 900;
                position: absolute;
                z-index: 0;
                left: 40%;
                padding: 10px 10px 10px 10px;    
                color: #6a2b96;
            }

            /* ###############################TableSection################################### */

            td{
                
                color: #6a2b96;
                font-weight: 800;
            }
            table{
                background-color: white;
                min-width: 700px;
                border-collapse:separate;
                border-spacing:10px 15px;
                border-color: #6a2b96;
            }

            /* ############################################################################# */

            .inventory
            {
                // background-color: white;
                width: 700px;
                margin-top: 130px;
                margin-left: 130px;
                

            }
            .bookimg
            {
                width: 100px;
                height: 120px;
            }
            /* ########################## button ########################## */
            .nsbtn
            {
                margin-bottom: 10px;
                margin-left: 10px;
                margin-right: 10px;
                font-weight: bold;
                border: 0;
                padding-left: 10px;
                padding-right: 10px;
                height: 25px;
                background-color: #4a1e69;
                color: white;
            }
            .nsaddcart
            {
                background-color: #74D600 !important;
            }

            .nshandlerbtn{
                float: right;
            }


            /* ##########################footer########################## */
            .nsfooter
            {
                height: 40px;
            }

        </style>
        <style>
            .logotitle{
                display:inline-block;
                color:#4a1e69;

                margin-top: 7px;
                margin-left: 50px;
                font-size: 60;
                width: 600px;
                font-weight: 1000;
            }
            .logoimage{
                width: 100px;
                height: 100px;

            }
            .logo
            {
                padding: 10px 10px 10px 10px;
                background-color:#4a1e69;
                position: relative;
                top: -60px;
            }
            .tabsindex
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 20px; 
                padding-right: 30px; 

                margin-top: 7px;
            }
            .nsheaderindex{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 10px;

                display:inline-block;

            }
            .logoquote{
                display:inline-block;
                background-color: white;
                margin-top: -30px;
                border: solid;
                padding: 10px;
                border-color: #4a1e69;
                border-width: 5px;
                width: 200px;
                height: 60px;
                vertical-align: middle;
                margin-left: -0px; 
                font-family:  "Comic Sans MS";
                font-size: 15PX;
                text-align: center;
                font-weight: 900;
                color: black;
                padding: 10px 10px 10px 10px;
            }



        </style>

        <link rel="stylesheet" type="text/css" href="css/dropdown.css">
    </head>
    <body>
        <?php

        session_start();
        if(!$_SESSION["uname"])
        {
            header("Location: index.php");
        }
        else
        {

            $uname=$_SESSION["uname"];
        }
        ?>

        <div>
            <div>
                <input class="nsbtn nshandlerbtn" type="button" value="Logout" onclick="location.href = 'logout.php'">
                <center><div class="logotitle"><a href="index.php">Novel Store</a></div></center>
            </div>
            <div class="nsheader">
                <div class="logo"><img src="images/logoimage.png" class="logoimage"></div>
                <div class="tabsindex"><?php include 'include/accountdd.php';?></div>
                <div class="tabsindex"><a href="#">Contact us</a></div>
                <div class="tabsindex"><?php include 'include/dropdown.php';?></div>
                <div class="tabsindex"><a href="welcome.php">Home</a></div>
            </div>
            <div class="nsbody">
                <div class="logoquote">A WORLD OF BOOKS IN THE PALM OF YOUR HAND</div>
                 <div class="nsbodyheader">INVENTORY</div>
                
                <div class="inventory">
                    <form method="get" action="insertcarttable.php">
                        <table>
                            <tr><td>Name</td><td>author</td><td>quantity</td><td>price</td><td>image</td></tr>
                            <?php

                            $servername = "localhost";
                            $username = "root";
                            $password = "";
                            $dbname = "novelstore";

                            // Create connection
                            $conn = new mysqli($servername, $username, $password, $dbname);
                            // Check connection
                            if ($conn->connect_error) {
                                die("Connection failed: " . $conn->connect_error);
                            } 

                            $sql = "SELECT id, bname, bauthor, quantity, price, imgpic FROM nsinventory";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo "<tr><td>". $row["bname"]. "</td>";
                                    echo "<td>" . $row["bauthor"]. "</td>";
                                    echo "<td>" . $row["quantity"]. "</td>";
                                    echo "<td>" . $row["price"]. "</td>";
                                    //<img src="H.gif" alt="" border=3 height=100 width=100></img>
                                    $a= ''.$row["imgpic"].'';
                                    echo '<td><img class="bookimg" src="images/'.$a.'" alt="xyz 1 '.$a.'"></td><td><input id="btnadd" class="nsaddcart nsbtn" type="submit" value="Add to cart" name="'.$row["id"].'" ></td></tr>';
                                }
                            } else {
                                echo "0 results";
                            }
                            $conn->close();

                            ?>

                        </table>
                    </form>

                </div>
                <div class="nsfooter">
                </div>
            </div>
        </div>

    </body>
</html>