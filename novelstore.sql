-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 23, 2018 at 06:44 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `novelstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `admintable`
--

CREATE TABLE IF NOT EXISTS `admintable` (
`id` int(10) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `dob` varchar(30) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `phno` bigint(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `address` varchar(200) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admintable`
--

INSERT INTO `admintable` (`id`, `fname`, `lname`, `dob`, `gender`, `phno`, `username`, `email`, `password`, `address`) VALUES
(1, 'vishald', 'dindalkop', '25-09-94', 'male', 2147483647, 'admin', 'vishald@gmail.com', 'admin', 'dharwad'),
(2, 'vishald', 'dindalkop', '25-09-94', 'male', 7204248834, 'admin', 'vishald@gmail.com', 'admin', 'dharwad');

-- --------------------------------------------------------

--
-- Table structure for table `billdetails`
--

CREATE TABLE IF NOT EXISTS `billdetails` (
`billid` int(100) NOT NULL,
  `amount` bigint(200) NOT NULL,
  `uid` int(100) NOT NULL,
  `uname` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `billdetails`
--

INSERT INTO `billdetails` (`billid`, `amount`, `uid`, `uname`) VALUES
(75, 700, 1, 'vishald'),
(76, 500, 1, 'vishald');

-- --------------------------------------------------------

--
-- Table structure for table `carttable`
--

CREATE TABLE IF NOT EXISTS `carttable` (
`citemid` int(100) NOT NULL,
  `bid` int(100) NOT NULL,
  `uid` int(100) NOT NULL,
  `bname` varchar(20) NOT NULL,
  `bauthor` varchar(20) NOT NULL,
  `quantity` int(20) NOT NULL,
  `price` bigint(100) NOT NULL,
  `total` bigint(200) NOT NULL,
  `imgpic` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customertable`
--

CREATE TABLE IF NOT EXISTS `customertable` (
`id` int(10) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `dob` varchar(30) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `phno` bigint(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `address` varchar(200) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `customertable`
--

INSERT INTO `customertable` (`id`, `fname`, `lname`, `dob`, `gender`, `phno`, `username`, `email`, `password`, `address`) VALUES
(1, 'vishal', 'dindalkop', '25-09-1994', 'male', 2147483647, 'vishald', 'vishald@gmail.com', 'vishald', 'vishald');

-- --------------------------------------------------------

--
-- Table structure for table `nsinventory`
--

CREATE TABLE IF NOT EXISTS `nsinventory` (
`id` int(20) NOT NULL,
  `bname` varchar(20) NOT NULL,
  `bauthor` varchar(20) NOT NULL,
  `quantity` int(20) NOT NULL,
  `price` bigint(100) NOT NULL,
  `imgpic` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `nsinventory`
--

INSERT INTO `nsinventory` (`id`, `bname`, `bauthor`, `quantity`, `price`, `imgpic`) VALUES
(53, 'Harrypotter', 'jkrowlin', 10, 250, 'harrypotterandthephilosophersstone.jpg'),
(54, 'A Brief History of T', 'Carl Sagan', 22, 500, 'stephenhawking.jpg'),
(55, 'The Alchemist', 'panlo corlho', 50, 700, 'thealchemist.jpg'),
(56, 'The Imortals of Melu', 'Amish', 12, 1000, 'theimmortalofmeluha.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE IF NOT EXISTS `orderdetails` (
  `citemid` int(100) NOT NULL,
  `billid` int(100) NOT NULL,
  `bid` int(100) NOT NULL,
  `uid` int(100) NOT NULL,
  `bname` varchar(20) NOT NULL,
  `bauthor` varchar(20) NOT NULL,
  `quantity` int(20) NOT NULL,
  `price` bigint(100) NOT NULL,
  `total` bigint(200) NOT NULL,
  `imgpic` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderdetails`
--

INSERT INTO `orderdetails` (`citemid`, `billid`, `bid`, `uid`, `bname`, `bauthor`, `quantity`, `price`, `total`, `imgpic`) VALUES
(5, 23, 49, 1, 'qwert', 'qwert', 1, 0, 0, 'word-aasd-asd-asd-asd.png'),
(6, 23, 50, 1, 'asd', 'asd', 1, 2000, 2000, 'word-aasd-asd-asd-asd.png'),
(5, 63, 49, 1, 'qwert', 'qwert', 1, 0, 0, 'word-aasd-asd-asd-asd.png'),
(6, 63, 50, 1, 'asd', 'asd', 1, 2000, 2000, 'word-aasd-asd-asd-asd.png'),
(5, 64, 49, 1, 'qwert', 'qwert', 1, 0, 0, 'word-aasd-asd-asd-asd.png'),
(6, 64, 50, 1, 'asd', 'asd', 1, 2000, 2000, 'word-aasd-asd-asd-asd.png'),
(5, 65, 49, 1, 'qwert', 'qwert', 1, 0, 0, 'word-aasd-asd-asd-asd.png'),
(6, 65, 50, 1, 'asd', 'asd', 1, 2000, 2000, 'word-aasd-asd-asd-asd.png'),
(5, 66, 49, 1, 'qwert', 'qwert', 2, 0, 0, 'word-aasd-asd-asd-asd.png'),
(6, 66, 50, 1, 'asd', 'asd', 2, 2000, 4000, 'word-aasd-asd-asd-asd.png'),
(1, 67, 49, 1, 'qwert', 'qwert', 1, 0, 0, 'word-aasd-asd-asd-asd.png'),
(2, 67, 50, 1, 'asd', 'asd', 1, 2000, 2000, 'word-aasd-asd-asd-asd.png'),
(3, 67, 51, 1, 'asd', 'asd', 1, 1000, 1000, 'word-aasd-asd-asd-asd.png'),
(4, 67, 52, 1, 'asd', 'asd', 1, 123, 123, 'word-aasd-asd-asd-asd.png'),
(1, 68, 49, 1, 'qwert', 'qwert', 1, 0, 0, 'word-aasd-asd-asd-asd.png'),
(2, 68, 50, 1, 'asd', 'asd', 1, 2000, 2000, 'word-aasd-asd-asd-asd.png'),
(3, 68, 51, 1, 'asd', 'asd', 1, 1000, 1000, 'word-aasd-asd-asd-asd.png'),
(4, 68, 52, 1, 'asd', 'asd', 1, 123, 123, 'word-aasd-asd-asd-asd.png'),
(5, 69, 51, 1, 'asd', 'asd', 1, 1000, 0, 'word-aasd-asd-asd-asd.png'),
(6, 69, 50, 1, 'asd', 'asd', 1, 2000, 0, 'word-aasd-asd-asd-asd.png'),
(7, 69, 52, 1, 'asd', 'asd', 1, 123, 0, 'word-aasd-asd-asd-asd.png'),
(8, 70, 50, 1, 'asd', 'asd', 2, 2000, 0, 'word-aasd-asd-asd-asd.png'),
(9, 71, 52, 1, 'asd', 'asd', 2, 123, 0, 'word-aasd-asd-asd-asd.png'),
(1, 72, 49, 1, 'qwert', 'qwert', 1, 0, 0, 'word-aasd-asd-asd-asd.png'),
(2, 72, 50, 1, 'asd', 'asd', 1, 2000, 0, 'word-aasd-asd-asd-asd.png'),
(3, 73, 50, 1, 'asd', 'asd', 1, 2000, 0, 'word-aasd-asd-asd-asd.png'),
(4, 73, 51, 1, 'asd', 'asd', 1, 1000, 0, 'word-aasd-asd-asd-asd.png'),
(1, 74, 53, 1, 'Harrypotter', 'jkrowlin', 1, 250, 0, 'harrypotterandthephilosophersstone.jpg'),
(2, 74, 54, 1, 'A Brief History of T', 'Carl Sagan', 1, 0, 0, 'stephenhawking.jpg'),
(3, 74, 55, 1, 'The Alchemist', 'panlo corlho', 1, 0, 0, 'thealchemist.jpg'),
(4, 75, 56, 1, 'The Imortals of Melu', 'Amish', 1, 1000, 0, 'theimmortalofmeluha.jpg'),
(5, 75, 55, 1, 'The Alchemist', 'panlo corlho', 1, 700, 0, 'thealchemist.jpg'),
(6, 76, 53, 1, 'Harrypotter', 'jkrowlin', 2, 250, 500, 'harrypotterandthephilosophersstone.jpg'),
(7, 76, 54, 1, 'A Brief History of T', 'Carl Sagan', 1, 500, 500, 'stephenhawking.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admintable`
--
ALTER TABLE `admintable`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billdetails`
--
ALTER TABLE `billdetails`
 ADD PRIMARY KEY (`billid`);

--
-- Indexes for table `carttable`
--
ALTER TABLE `carttable`
 ADD PRIMARY KEY (`citemid`);

--
-- Indexes for table `customertable`
--
ALTER TABLE `customertable`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nsinventory`
--
ALTER TABLE `nsinventory`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admintable`
--
ALTER TABLE `admintable`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `billdetails`
--
ALTER TABLE `billdetails`
MODIFY `billid` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `carttable`
--
ALTER TABLE `carttable`
MODIFY `citemid` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customertable`
--
ALTER TABLE `customertable`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nsinventory`
--
ALTER TABLE `nsinventory`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
