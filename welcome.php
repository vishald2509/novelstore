
<html>
    <head>
        <title>Weclome</title>
        <style>

            /* ##########################Main########################## */

            /* ##########################Header########################## */
            .nsheader{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 40px;

                display:inline-block;

            }
            .tabs
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 100px;
                padding-right: 100px;

                margin-top: 7px;
            }
            .logo{
                display:inline-block;
                color:whitesmoke;
                margin-top: 7px;
                margin-left: 50px;
                font-size: 20;
                font-weight: 1000;
            }
            /* ##########################Body########################## */
            a{
                color: inherit;
                text-decoration: none;
            }
            .nsbody{
                background-color: #6a2b96;
                height: 520px;
                margin-top: 40px;
                margin-bottom: 40px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .nsbodyleft{
                display:inline-block;
                height: 100%;              
            }
            .nsbodyleftquote{
                display:inline-block;
                background-color: white;
                margin-top: -20px;
                width: 350px;
                height: 60px;
                vertical-align: middle;
                margin-left: 100px;
                font-family:  "Comic Sans MS";
                font-size: 20PX;
                text-align: center;
                font-weight: 900;
                padding: 10px 10px 10px 10px;
            }


            /* ##########################loginpart########################## */
            .nsbodyright{
                display:inline-block;
                width: 400px;
                float: right;
                background-color: white;
                height: 100%;
                color: black;
            }
            .fontblack{
                color: black;
            }
            .nslogintable
            {
                margin: 150px 0px 150px 80px;
            }
            .nsloginhead 
            {
                font-weight: bold;
                font-size: 35px;
            }

            .nsfontsmaller
            {
                font-size: 15px;
            }
            .nsloginlabel
            {
                font-weight: bold;
            }

            /* ##########################loginpart########################## */


            /* ##########################footer########################## */

            .nsbtn
            {
                font-weight: bold;
                border: 0;
                width: 80px;
                height: 25px;
                background-color: #4a1e69;
                color: white;
            }
            .nshandlerbtn{
                float: right;
            }

            .nsfooter
            {
                height: 40px;
            }
        </style>

        <style>
            .logotitle{
                display:inline-block;
                color:#4a1e69;

                margin-top: 7px;
                margin-left: 50px;
                font-size: 60;
                width: 600px;
                font-weight: 1000;
            }
            .logoimage{
                width: 100px;
                height: 100px;

            }
            .logo
            {
                padding: 10px 10px 10px 10px;
                background-color:#4a1e69;
                position: relative;
                top: -60px;
            }
            .tabsindex
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 20px; 
                padding-right: 30px; 

                margin-top: 7px;
            }
            .nsheaderindex{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 10px;

                display:inline-block;

            }
            .logoquote{
                display:inline-block;
                background-color: white;
                margin-top: -30px;
                border: solid;
                padding: 10px;
                border-color: #4a1e69;
                border-width: 5px;
                width: 200px;
                height: 60px;
                vertical-align: middle;
                margin-left: -0px; 
                font-family:  "Comic Sans MS";
                font-size: 15PX;
                text-align: center;
                font-weight: 900;
                color: black;
                padding: 10px 10px 10px 10px;
            }



        </style>
        <link rel="stylesheet" type="text/css" href="css/dropdown.css">


    </head>
    <body>
        <?php

        session_start();
        if(!$_SESSION["uname"])
        {
            header("Location: index.php");
        }
        else
        {
            $uname=$_SESSION["uname"];
            $user_id=$_SESSION["uid"];
        }
        ?>
        <div>
            <input class="nsbtn nshandlerbtn" type="button" value="Logout" onclick="location.href = 'logout.php'">
            <center><div class="logotitle"><a href="index.php">Novel Store</a></div></center>
        </div>
        <div>
            <div class="nsheaderindex">
                <div class="logo"><img src="images/logoimage.png" class="logoimage"></div>
                <div class="tabsindex"><?php include 'include/accountdd.php';?></div>
                <div class="tabsindex"><a href="#">Contact us</a></div>
                <div class="tabsindex"><?php include 'include/dropdown.php';?></div>
                <div class="tabsindex"><a href="welcome.php">Home</a></div>
            </div>
            <div class="nsbody">
                <div class="nsbodyleft">
                    <div class="logoquote">A WORLD OF BOOKS IN THE PALM OF YOUR HAND</div>
                </div>
                <div class="nsbodyright">
                    Welcome <?php echo $uname; echo $user_id; ?> to our website how can we help you?
                </div>
            </div>
            <div class="nsfooter">

            </div>
        </div>
    </body>

</html>