<html>
    <head>
        <style>
            .addbookheading
            {
                color: white;
            }
        </style>
        <style>
            .logotitle{
                display:inline-block;
                color:#4a1e69;

                margin-top: 7px;
                margin-left: 50px;
                font-size: 60;
                width: 600px;
                font-weight: 1000;
            }
            .logoimage{
                width: 100px;
                height: 100px;

            }
            .logo
            {
                padding: 10px 10px 10px 10px;
                background-color:#4a1e69;
                position: relative;
                top: -60px;
            }
            .tabsindex
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 20px; 
                padding-right: 30px; 

                margin-top: 7px;
            }
            .nsheaderindex{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 10px;

                display:inline-block;

            }
            .logoquote{
                display:inline-block;
                background-color: white;
                margin-top: -30px;
                border: solid;
                padding: 10px;
                border-color: #4a1e69;
                border-width: 5px;
                width: 200px;
                height: 60px;
                vertical-align: middle;
                margin-left: -0px; 
                font-family:  "Comic Sans MS";
                font-size: 15PX;
                text-align: center;
                font-weight: 900;
                color: black;
                padding: 10px 10px 10px 10px;
            }



        </style>
        <link rel="stylesheet" type="text/css" href="css/nsstyle.css">
        <script>
            function loadthis()
            {
                <?php
                session_start();
                if(isset($_SESSION["error"]))
                {
                    $warning = $_SESSION["error"];

                }else
                {
                    $warning = "#FFFFFF";
                }


                ?>
                document.getElementById("imgwarning").style.color="<?php echo $warning; ?>";

            }
        </script>
    </head>
    <body onload="loadthis()">
        <div>
            <input class="nsbtn nshandlerbtn" type="button" value="Logout" onclick="location.href = 'logout.php'">
            <input class="nsbtn nshandlerbtn" type="button" value="Signup" onclick="location.href = 'adminsignup.php'">
            <center><div class="logotitle"><a href="index.php">Novel Store</a></div></center>

        </div>

        <div>
            <div class="nsheaderindex">
                <div class="logo"><img src="images/logoimage.png" class="logoimage"></div>
                <div class="tabsindex"><a href="adminaccount.php">Account</a></div>
                <div class="tabsindex"><a href="#">Contact us</a></div>
                <div class="tabsindex"><a href="adminmanagement.php">Inventory</a></div>
                <div class="tabsindex"><a href="welcome.php">Home</a></div>
            </div>
            <div class="nsbody">
                <div class="nsbodyleft">
                    <div class="logoquote">A WORLD OF BOOKS IN THE PALM OF YOUR HAND</div>    
                    <div class="uploader">
                        <h3 class="addbookheading">Upload new book to the inventory</h3><br>
                        <form enctype="multipart/form-data" action="upload.php" method="POST">
                            <table>
                                <tr><td><label>Name of the Book:</label></td><Td><input  type="text" name="name" ></Td></tr>
                                <tr><td><label>Book Author:</label></td><td><input type="text" name="author" ></td></tr>
                                <tr><Td><label>Quantity:</label></Td><td><input type="text" name="quantity" ></td></tr>
                                <tr><Td><label>Price:</label></Td><td><input type="text" name="price" ></td></tr>
                                <tr><Td><input type="hidden" name="MAX_FILE_SIZE" value="512000" /><label>Upload image:</label></Td><td><input class="nsbtn" name="userfile" type="file" /><label id="imgwarning">image file should be 500kb or smaller size</label></td></tr>
                                <tr><td></td><td ><input class="nsbtn" type="submit" value="ADD BOOK" /></td></tr>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="nsbodyright">


                </div> 

            </div>
            <div class="nsfooter">

            </div>
        </div>
    </body>

</html>