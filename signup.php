<html>
    <head>
        <style>

            /* ##########################Main########################## */

            /* ##########################Header########################## */
            .nsheader{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 40px;

                display:inline-block;

            }
            .tabs
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 100px;
                padding-right: 100px;

                margin-top: 7px;
            }          .logo{
                display:inline-block;
                color:whitesmoke;
                margin-top: 7px;
                margin-left: 50px;
                font-size: 20;
                font-weight: 1000;
            }
            /* ##########################Body########################## */
            a{
                color: inherit;
                text-decoration: none;
            }
            .nsbody{
                background-color: #6a2b96;
                height: 520px;
                margin-top: 40px;
                margin-bottom: 40px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .nsbodyleft{
                display:inline-block;
                height: 100%;    
                color: white;
                padding: 10px 10px 10px 10px;
            }

            /* ###############################TableSection################################### */

            td{
                color: white;
            }
            table{
                border-collapse:separate;
                border-spacing:10px 15px;
            }

            /* ############################################################################# */

            .nsbodyleftquote{
                display:inline-block;
                background-color: white;
                margin-top: -20px;
                width: 350px;
                height: 60px;
                vertical-align: middle;
                margin-left: 100px;
                font-family:  "Comic Sans MS";
                font-size: 20PX;
                text-align: center;
                font-weight: 900;
                padding: 10px 10px 10px 10px;
            }
            /* ##########################loginpart########################## */
            .nsbodyright{
                display:inline-block;
                width: 400px;
                float: right;
                background-color: white;
                height: 100%;
                color: black;
            }
            .fontblack{
                color: black;
            }
            .nslogintable
            {
                margin: 150px 0px 150px 80px;
            }
            .nsloginhead 
            {
                font-weight: bold;
                font-size: 35px;
            }

            .nsfontsmaller
            {
                font-size: 15px;
            }
            .nsloginlabel
            {
                font-weight: bold;
            }

            /* ##########################loginpart########################## */



        </style>
        
        <style>
            .logotitle{
                display:inline-block;
                color:#4a1e69;

                margin-top: 7px;
                margin-left: 50px;
                font-size: 60;
                width: 600px;
                font-weight: 1000;
            }
            .logoimage{
                width: 100px;
                height: 100px;

            }
            .logo
            {
                padding: 10px 10px 10px 10px;
                background-color:#4a1e69;
                position: relative;
                top: -60px;
            }
            .tabsindex
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 20px; 
                padding-right: 30px; 

                margin-top: 7px;
            }
            .nsheaderindex{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 10px;

                display:inline-block;

            }
            .logoquote{
                display:inline-block;
                background-color: white;
                margin-top: -38px;
                border: solid;
                padding: 10px;
                border-color: #4a1e69;
                border-width: 5px;
                width: 200px;
                height: 60px;
                vertical-align: middle;
                margin-left: -0px; 
                font-family:  "Comic Sans MS";
                font-size: 15PX;
                text-align: center;
                font-weight: 900;
                color: black;
                padding: 10px 10px 10px 10px;
            }



        </style>
            <link rel="stylesheet" type="text/css" href="css/nsstyle.css">
    </head>
    <body>
        <div>
            <div>
                <input class="nsbtn nshandlerbtn" type="button" value="Admin" onclick="location.href = 'admin.php'">
                <center><div class="logotitle"><a href="index.php">Novel Store</a></div></center>
            </div>
            <div class="nsheaderindex">
                <div class="logo"><img src="images/logoimage.png" class="logoimage"></div>
                <div class="tabsindex"><a href="#">Account</a></div>
                <div class="tabsindex"><a href="#">Contact us</a></div>
                <div class="tabsindex"><a href="index.php">Inventory</a></div>
                <div class="tabsindex"><a href="index.php">Home</a></div>
            </div>
            <div class="nsbody">
                
                <div class="nsbodyleft">
                    <div class="logoquote">A WORLD OF BOOKS IN THE PALM OF YOUR HAND</div>
                
                    <form action="signupinsert.php" method="get">
                        <table >
                            <tr>
                                <td><label>First Name: </label></td><td><input type="text" name="fname"></td>
                                <td><label>Last Name: </label></td><td><input type="text" name="lname"></td>
                            </tr>
                            <tr><td colspan=""><label>Date of birth:</label></td><td colspan="3"><input type="text" name="dob"></td></tr>
                            <tr><td colspan=""><label>Gender:</label></td><td colspan="3"><input type="radio" name="gender" value="male"><label>Male</label><input type="radio" name="gender" value="female"><label>Female</label></td></tr>
                            <tr><td colspan=""><label>Phone No:</label></td><td colspan="3"><input type="text" name="phno"></td></tr>
                            <tr>
                                <td><label>UserName:</label></td><td><input type="text" name="username"></td>
                                <td><label>Email:</label></td><td><input type="text" name="email"></td>
                            </tr>
                            <tr>
                                <td><label>Password: </label></td><td><input type="password" name="pass"></td>
                                <td><label>Verifie Password: </label></td><td><input type="password" name="vpass"></td>
                            </tr>

                            <tr><td colspan=""><label>Address:</label></td><td colspan="3"><textarea rows="4" cols="35" name="address"></textarea></td></tr>
                            <tr><td colspan=""></td><td><input type="submit" value="Submit" class="nsbtn"> </td><td><input type="reset" class="nsbtn" ></td></tr>
                        </table>                    
                    </form>
                </div>
                <div class="nsbodyright">
                    <form action="login.php" method="get">
                        <table class="nslogintable">
                            <tr ><td colspan="3" class="fontblack nsloginhead"><label>Login <span class="nsfontsmaller">to stay connected</span></label></td></tr>
                            <tr>
                                <td class="fontblack nsloginlabel" ><label>UserName</label></td>
                                <td class="fontblack nslogininput"><input type="text" name="username" ></td>
                                <td class="fontblack nsloginerror"></td>
                            </tr>
                            <tr>
                                <td class="fontblack nsloginlabel" ><label>Password</label></td>
                                <td class="fontblack nslogininput"><input type="password" name="password" ></td>
                                <td class="fontblack nsloginerror"></td>
                            </tr>

                            <tr>
                                <td class="fontblack nsloginlabel" ></td>
                                <td class="fontblack nslogininput">
                                    <input type="submit" value="Login" class="nsbtn"> 
                                    <input type="button" value="Signup" class="nsbtn" onclick="location.href = 'signup.php'">
                                </td>
                                <td class="fontblack nsloginerror"></td>
                            </tr>
                        </table>
                    </form>
                </div> 

            </div>
            <div class="nsfooter">

            </div>
        </div>
    </body>

</html>