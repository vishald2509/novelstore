<html>
    <head>
        <title>AdminHandler</title>
        <style>

            /* ##########################Main########################## */

            /* ##########################Header########################## */
            .nsheader{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 40px;

                display:inline-block;

            }
            .tabs
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 100px;
                padding-right: 100px;

                margin-top: 7px;
            }
            .logo{
                display:inline-block;
                color:whitesmoke;
                margin-top: 7px;
                margin-left: 50px;
                font-size: 20;
                font-weight: 1000;
            }
            /* ##########################Body########################## */
            a{
                color: inherit;
                text-decoration: none;
            }
            .nsbody{
                background-color: #6a2b96;
                height: 520px;
                margin-top: 40px;
                margin-bottom: 40px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .nsbodyleft{
                display:inline-block;
                height: 100%;              
            }
            .nsbodyleftquote{
                display:inline-block;
                background-color: white;
                margin-top: -20px;
                width: 350px;
                height: 60px;
                vertical-align: middle;
                margin-left: 100px;
                font-family:  "Comic Sans MS";
                font-size: 20PX;
                text-align: center;
                font-weight: 900;
                padding: 10px 10px 10px 10px;
            }


            /* ##########################loginpart########################## */
            .nsbodyright{
                display:inline-block;
                width: 400px;
                float: right;
                background-color: white;
                height: 100%;
                color: black;
            }
            .fontblack{
                color: black;
            }
            .nslogintable
            {
                margin: 150px 0px 150px 80px;
            }
            .nsloginhead 
            {
                font-weight: bold;
                font-size: 35px;
            }

            .nsfontsmaller
            {
                font-size: 15px;
            }
            .nsloginlabel
            {
                font-weight: bold;
            }
            .nsfontadministrator
            {
                font-size: 20px;
            }

            /* ##########################loginpart########################## */



            /* ########################## button ########################## */
            .nsbtn
            {
                margin-bottom: 10px;
                margin-left: 10px;
                margin-right: 10px;
                font-weight: bold;
                border: 0;
                width: 80px;
                height: 25px;
                background-color: #4a1e69;
                color: white;
            }

            .nshandlerbtn{
                float: right;
            }
            /* ##########################footer########################## */
            .nsfooter
            {
                height: 40px;
            }
        </style>
        <style>
            .logotitle{
                display:inline-block;
                color:#4a1e69;

                margin-top: 7px;
                margin-left: 50px;
                font-size: 60;
                width: 600px;
                font-weight: 1000;
            }
            .logoimage{
                width: 100px;
                height: 100px;

            }
            .logo
            {
                padding: 10px 10px 10px 10px;
                background-color:#4a1e69;
                position: relative;
                top: -60px;
            }
            .tabsindex
            {

                display:inline-block;
                float: right;
                color: white;
                font-weight: 600;
                padding-left: 20px; 
                padding-right: 30px; 

                margin-top: 7px;
            }
            .nsheaderindex{
                background-color: #6a2b96;
                height: 30px;
                width: 100%;
                margin-top: 10px;

                display:inline-block;

            }
            .logoquote{
                display:inline-block;
                background-color: white;
                margin-top: -30px;
                border: solid;
                padding: 10px;
                border-color: #4a1e69;
                border-width: 5px;
                width: 200px;
                height: 60px;
                vertical-align: middle;
                margin-left: -0px; 
                font-family:  "Comic Sans MS";
                font-size: 15PX;
                text-align: center;
                font-weight: 900;
                color: black;
                padding: 10px 10px 10px 10px;
            }



        </style>
    </head>
    <body>
        <div>
            <input class="nsbtn nshandlerbtn" type="button" onclick="location.href = 'index.php'" value="User">
            <center><div class="logotitle"><a href="index.php">Novel Store</a></div></center>
        </div>
        <div>
            <div class="nsheaderindex">
                <div class="logo"><img src="images/logoimage.png" class="logoimage"></div>
                <div class="tabsindex"><a href="admin.php">Account</a></div>
                <div class="tabsindex"><a href="#">Contact us</a></div>
                <div class="tabsindex"><a href="admin.php">Inventory</a></div>
                <div class="tabsindex"><a href="admin.php">Home</a></div>
            </div>
            <div class="nsbody">
                <div class="nsbodyleft">
                    <div class="logoquote">A WORLD OF BOOKS IN THE PALM OF YOUR HAND</div>
                </div>
                <div class="nsbodyright">
                    <form action="adminlogin.php" method="get">
                        <table class="nslogintable">
                            <tr ><td colspan="3" class="fontblack nsfontadministrator"><label>ADMINISTRATOR</label></td></tr>
                            <tr ><td colspan="3" class="fontblack nsloginhead"><label>Login <span class="nsfontsmaller">to stay connected</span></label></td></tr>
                            <tr>
                                <td class="fontblack nsloginlabel" ><label>UserName</label></td>
                                <td class="fontblack nslogininput"><input type="text" name="username" ></td>
                                <td class="fontblack nsloginerror"></td>
                            </tr>
                            <tr>
                                <td class="fontblack nsloginlabel" ><label>Password</label></td>
                                <td class="fontblack nslogininput"><input type="password" name="password" ></td>
                                <td class="fontblack nsloginerror"></td>
                            </tr>

                            <tr>
                                <td class="fontblack nsloginlabel" ></td>
                                <td class="fontblack nslogininput">
                                    <input type="submit" value="Login" class="nsbtn"> 
                                </td>
                                <td class="fontblack nssignup"></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
            <div class="nsfooter">

            </div>
        </div>
    </body>

</html>